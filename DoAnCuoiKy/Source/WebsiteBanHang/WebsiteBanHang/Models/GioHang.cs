﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebsiteBanHang.Models
{
    public class GioHang
    {
        public SanPham ctgh_SanPham { get; set; }
        public int ctgh_SoLuong { get; set; }
        public TaiKhoan ctgh_KhachHang { get; set; }
        public int ctgh_TongTien { get; set; }
        public int ctgh_Tong = 0;
    }
}