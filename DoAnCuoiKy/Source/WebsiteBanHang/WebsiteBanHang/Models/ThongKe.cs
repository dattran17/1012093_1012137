﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebsiteBanHang.Models
{
    public class ThongKe
    {
        public List<ThongTinThongKe> All { get; set; }
        public List<ThongTinThongKe> Nam { get; set; }
        public List<ThongTinThongKe> Quy { get; set; }
        public List<ThongTinThongKe> Thang { get; set; }
        public List<ThongTinThongKe> Ngay { get; set; }
      
    }
}