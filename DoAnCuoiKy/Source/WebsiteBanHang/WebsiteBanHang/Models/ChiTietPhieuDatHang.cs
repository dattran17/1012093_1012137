//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebsiteBanHang.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ChiTietPhieuDatHang
    {
        public int ctpdh_MaPDH { get; set; }
        public int ctpdh_MaSanPham { get; set; }
        public int ctpdh_Gia { get; set; }
        public int ctpdh_SoLuong { get; set; }
    }
}
