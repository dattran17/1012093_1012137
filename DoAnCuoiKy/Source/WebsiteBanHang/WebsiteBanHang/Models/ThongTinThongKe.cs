﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebsiteBanHang.Models
{
    public class ThongTinThongKe
    {
        public SanPham tk_SanPham { get; set; }
        public int tk_SoLuong { get; set; }
        public int tk_MaKhachHanh { get; set; }
        public int tk_TongTien { get; set; }
        public int tk_Tong { get; set; }
    }
}