﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebsiteBanHang.Models;
namespace WebsiteBanHang.Controllers
{
    public class GuiTarController : BaseController
    {
        WebSiteGuitarEntities db = new WebSiteGuitarEntities();

        #region[INDEX]
        public ActionResult Index()
        {

            IndextProduct ip = new IndextProduct();
            var productNew = (from p in db.SanPham orderby p.sp_Id descending select p).Take(8).ToList();
            for (int i = 0; i < productNew.Count; i++)
            {
                ip.New.Add(productNew[i]);
               
            }
           
            var productFeature = (from p in db.SanPham select p).ToList();
            for (int i = 0; i < productFeature.Count; i++)
            {
                ip.Fav.Add(productFeature[i]);
               
            }
            ViewBag.Name = BienToanCuc.Bien;
            return View(ip);
        }
        #endregion

        #region[CREATE]
        public ActionResult Create()
        {

            ViewBag.Name = BienToanCuc.Bien;
            return View();
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection,SanPham sanpham)
        {
            string Ten = collection["txtTen"];
            int Gia = int.Parse(collection["txtGia"]);
            int SoLuong = int.Parse(collection["txtSoLuong"]);
            string Loai = collection["txtLoai"];
            string MauSac = (collection["txtMauSac"]);
            string HA = collection["txtHA"];
           
            sanpham.sp_TenSanPham = Ten;
            sanpham.sp_GiaTien = Gia;
            sanpham.sp_Loai = Loai;
            sanpham.sp_HinhAnh = HA;
            sanpham.sp_MauSac = MauSac;
            sanpham.sp_SoLuong = SoLuong;
            db.SanPham.Add(sanpham);

            db.SaveChanges();
            return RedirectToAction("Update");
        }


        #endregion
        public ActionResult DeleteAll()
        {
            for (int i = 0; i < db.ChiTietGioHang.ToList().Count(); i++)
            {
                db.ChiTietGioHang.Remove(db.ChiTietGioHang.ToList()[i]);

            }
            db.SaveChanges();
            return RedirectToAction("Cart");
        }

        #region[Update]
        public ActionResult Update()
        {
            String chuoi="";
            List<SanPham> list = new List<SanPham>();
             var Listsp = (from p in db.SanPham select p).ToList();
             for (int i = 0; i < Listsp.Count; i++)
             {
                 list.Add(Listsp[i]);
                 //chuoi += "<tr>";
                 //chuoi += "<td class=\"image\"><a href=\"#\"><img title=\"product\" alt=\"product\" src=\"" + Listsp [i].sp_HinhAnh+ "\" height=\"50\" width=\"50\"></a></td>";
                 //chuoi += "<td class=\"name\"><a href=\"#\">"+Listsp[i].sp_TenSanPham+"</a></td>";
                 //chuoi += "<td class=\"model\">"+Listsp[i].sp_Loai+"</td>";
                 //chuoi += "<td class=\"quantity\"><input type=\"text\" size=\"1\" value=\""+Listsp[i].sp_SoLuong+"\" name=\"quantity[40]\" class=\"span1\"></td>";
                 //chuoi += "<td class=\"price\">"+Listsp[i].sp_GiaTien+"</td>";
                 //chuoi += "<td class=\"total\">"+Listsp[i].sp_MauSac+"</td>";
                 //chuoi += "<td class=\"remove-update\">";
                 //chuoi += "<a href=\"#\" class=\"tip remove\" title=\"Remove\"><img src=\"images/remove.png\" alt=\"\"></a>";
                 //chuoi += " <a href=\"#\" class=\"tip update\" title=\"Update\"><img src=\"images/update.png\" alt=\"\"></a>";
                 //chuoi += "</td>";
                 //chuoi += "</tr>";
             }
            ViewBag.ListSanPham = chuoi;

            ViewBag.Name = BienToanCuc.Bien;
            return View(list);

        }
         #endregion

        #region[Edit]
        public ActionResult Edit(int id)
        {

            ViewBag.Name = BienToanCuc.Bien;
            var sp = db.SanPham.First(m => m.sp_Id == id);
            return View(sp);

        }
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            SanPham sp = db.SanPham.First(m =>m.sp_Id == id);
            sp.sp_TenSanPham = collection["sp_TenSanPham"];
            sp.sp_GiaTien = int.Parse(collection["sp_GiaTien"]);
            sp.sp_Loai = collection["sp_Loai"];
            sp.sp_SoLuong = int.Parse(collection["sp_SoLuong"]);
            sp.sp_MauSac = collection["sp_MauSac"];
            sp.sp_HinhAnh = collection["sp_HinhAnh"];
            UpdateModel(sp);
            db.SaveChanges();
            return RedirectToAction("Update");
        }
        #endregion

        #region[Delete]
       
        public ActionResult Delete(int id)
        {
            SanPham sp = db.SanPham.First(m => m.sp_Id == id);
            db.SanPham.Remove(sp);
            UpdateModel(sp);
            db.SaveChanges();
            return RedirectToAction("Update");
        }
        #endregion

        #region[List]

        public ActionResult List()
        {

            List<SanPham> List = new List<SanPham>();
            var ListSanPham = db.SanPham.ToList();
            List<SanPham> TempList = new List<SanPham>();
            TempList = (List<SanPham>)TempData["List"];
            String ThongBao=(String)TempData["TrangThai"];
            if (ThongBao !=  "Khong Thay")
            {
                if (TempList != null)
                {


                    for (int i = 0; i < TempList.Count(); i++)
                    {
                        List.Add(TempList[i]);
                    }
                }
                else
                {
                    for (int i = 0; i < ListSanPham.Count(); i++)
                    {
                        List.Add(ListSanPham[i]);
                    }
                }
            }
            else
            {
                ViewBag.ThongBao = TempData["TrangThai"];
            }
            ViewBag.Name = BienToanCuc.Bien;
            return View(List);
        }
        #endregion

        #region[Bass]

        public ActionResult Bass()
        {
            List<SanPham> List = new List<SanPham>();
            var ListSanPham = db.SanPham.ToList();
            for (int i = 0; i < ListSanPham.Count(); i++)
            {
                if (ListSanPham[i].sp_Loai == "Bass") 
                {
                    List.Add(ListSanPham[i]);
                }
            }

            ViewBag.Name = BienToanCuc.Bien;
            return View(List);

        }
        #endregion

        #region[Dien]

        public ActionResult Dien()
        {
            List<SanPham> List = new List<SanPham>();
            var ListSanPham = db.SanPham.ToList();
            for (int i = 0; i < ListSanPham.Count(); i++)
            {
                if (ListSanPham[i].sp_Loai == "Điện")
                {
                    List.Add(ListSanPham[i]);
                }
            }

            ViewBag.Name = BienToanCuc.Bien;
            return View(List);
        }
        #endregion

        #region[Classic]
        public ActionResult Classic()
        {
            List<SanPham> List = new List<SanPham>();
            var ListSanPham = db.SanPham.ToList();
            for (int i = 0; i < ListSanPham.Count(); i++)
            {
                if (ListSanPham[i].sp_Loai == "Classic")
                {
                    List.Add(ListSanPham[i]);
                }
            }

            ViewBag.Name = BienToanCuc.Bien;
            return View(List);
        }
        #endregion

        #region[Acaustic]
        public ActionResult Acaustic()
        {
            List<SanPham> List = new List<SanPham>();
            var ListSanPham = db.SanPham.ToList();
            for (int i = 0; i < ListSanPham.Count(); i++)
            {
                if (ListSanPham[i].sp_Loai == "Acaustic")
                {
                    List.Add(ListSanPham[i]);
                }
            }

            ViewBag.Name = BienToanCuc.Bien;
            return View(List);
        }
        #endregion
        
        #region[Login]
        public ActionResult Login(String username, String password)
        {
            var List = db.TaiKhoan.ToList();
            for (int i = 0; i < List.Count(); i++)
            {
                if ((List[i].tk_TenDangNhap == username) && (List[i].tk_MatKhau == password))
                {
                    BienToanCuc.tkdn=List[i];
                    if (List[i].tk_Loai == 0)
                    {
                        BienToanCuc.Bien = "admin";
                        return RedirectToAction("Index");

                    }
                    if (List[i].tk_Loai == 1)
                    {
                        BienToanCuc.Bien = "member";

                        return RedirectToAction("Index");
                    }

                }
            }
            return RedirectToAction("Index");
        }
        #endregion

        #region[SignUp]
        public ActionResult Success()
        {
            return View();
        }
        public ActionResult SignUp()
        {
            ViewBag.Name = BienToanCuc.Bien;
            return View();
        }
        [HttpPost]
        public ActionResult SignUp(FormCollection collection, TaiKhoan tk)
        {
            string Tendn = collection["tendangnhap"];
            string pass = collection["password"];
            string Ten = collection["tennguoidung"];
            string DiaChi = collection["diachi"];
            string sdt = (collection["sodienthoai"]);
            string mail = collection["email"];


            tk.tk_TenDangNhap = Tendn;
            tk.tk_MatKhau = pass;
            tk.tk_TenNguoiDung = Ten;
            tk.tk_DiaChi = DiaChi;
            tk.tk_SoDienThoai = sdt;
            tk.tk_Email = mail;
            tk.tk_Loai = 1;
            db.TaiKhoan.Add(tk);

            db.SaveChanges();
            return RedirectToAction("Success");
        }
        #endregion

        #region[Account]
        public ActionResult Account()
        {

            ViewBag.Name = BienToanCuc.Bien;
            return View(BienToanCuc.tkdn);
        }
        #endregion

        #region[Logout]
        public ActionResult Logout()
        {

             BienToanCuc.Bien ="visitor";
             return RedirectToAction("Index");
        }
        #endregion

        #region[Search]
        public ActionResult Search(String search)
        {

          //  BienToanCuc.spSearch = new List<SanPham>();
            TempData["List"] = null;
            List<SanPham> TempList = new List<SanPham>();
            if (search != null)
            {

                var List = db.SanPham.ToList();
                for (int i = 0; i < List.Count(); i++)
                {
                    if (List[i].sp_TenSanPham.ToLower().Contains(search.ToLower()) == true || List[i].sp_Loai.ToLower().Contains(search.ToLower()) == true
                        || (List[i].sp_GiaTien).ToString().Contains(search) == true)
                    {
                        TempList.Add(List[i]);
                    }
                }
                if (TempList != null)
                    TempData["List"] = TempList;
                else
                    TempData["TrangThai"] = "Khong Thay";
            }
         
            return RedirectToAction("List");
        }
        #endregion

        #region[Cart]

        public ActionResult DownCart(int id)
        {
            for (int i = 0; i < db.ChiTietGioHang.ToList().Count(); i++)
            {
                if (db.ChiTietGioHang.ToList()[i].ctgh_MaSanPham == id)
                {
                    db.ChiTietGioHang.ToList()[i].ctgh_SoLuong--;
                }
            }
            db.SaveChanges();
            return RedirectToAction("Cart"); ;
        }
        public ActionResult UpCart(int id)
        {
            for (int i = 0; i < db.ChiTietGioHang.ToList().Count(); i++)
            {
                if (db.ChiTietGioHang.ToList()[i].ctgh_MaSanPham == id)
                {
                    db.ChiTietGioHang.ToList()[i].ctgh_SoLuong++;
                }
            }
            db.SaveChanges();
            return RedirectToAction("Cart"); ;
        }
        public ActionResult Cart()
        {
            List<ChiTietGioHang> ctgh_List = db.ChiTietGioHang.ToList();
            List<SanPham> sp_List = db.SanPham.ToList();
            List<GioHang> gh = new List<GioHang>();
            int Tong = 0;
            for (int i = 0; i < ctgh_List.Count(); i++)
            {

                if (ctgh_List[i].ctgh_KhachHang == BienToanCuc.tkdn.tk_Id)
                {
                    GioHang Temp = new GioHang();
                    Temp.ctgh_SoLuong=ctgh_List[i].ctgh_SoLuong;
                    for (int j = 0; j < sp_List.Count(); j++)
                    {
                        if (ctgh_List[i].ctgh_MaSanPham == sp_List[j].sp_Id)
                        { 
                            Temp.ctgh_SanPham=sp_List[j];
                        }
                    }
                    Temp.ctgh_TongTien = Temp.ctgh_SoLuong * Temp.ctgh_SanPham.sp_GiaTien;
                    Tong = Tong + Temp.ctgh_TongTien;
                    Temp.ctgh_Tong = Tong;
                    gh.Add(Temp);
                }
            }
            BienToanCuc.GioHang = gh;
                return View(gh);
        }


        #endregion

        #region[Detail]

        public ActionResult Detail(int id)
        {
            CTSanPham ctsp = new CTSanPham();
            List<SanPham> ListSP = db.SanPham.ToList();
            for (int i = 0; i < ListSP.Count(); i++)
            {
                if (ListSP[i].sp_Id == id)
                {
                    ctsp.sp = new SanPham();
                    ctsp.sp = ListSP[i];
                }
            }
            String[] image = ctsp.sp.sp_HinhAnh.Split('.');
            ctsp.bigImages = image[0] + "b." + image[1];
            ViewBag.Name = BienToanCuc.Bien;
            return View(ctsp);
        }

        #endregion
        #region[AddCart]
        public int KiemTraGioHang(List<ChiTietGioHang> ctgh, int id)
        {
            for (int i = 0; i < ctgh.Count(); i++)
            {
                if (ctgh[i].ctgh_MaSanPham == id)
                {
                    return 1;
                }

            }
            return 0;
        }
        public ActionResult AddCart(int id)
        {
            List<ChiTietGioHang> ctgh =db.ChiTietGioHang.ToList();

            ChiTietGioHang Temp = new ChiTietGioHang(); ;

            if (KiemTraGioHang(ctgh, id) == 0)
            {
                Temp.ctgh_MaSanPham = id;
                Temp.ctgh_SoLuong = 1;
                Temp.ctgh_KhachHang = BienToanCuc.tkdn.tk_Id;
                db.ChiTietGioHang.Add(Temp);
            }
           
            //UpdateModel(Temp);
            db.SaveChanges();
            return RedirectToAction("Cart");
        }
        #endregion

        #region[DeleteCart]

        public ActionResult DeleteCart(int id)
        {
            ChiTietGioHang sp = db.ChiTietGioHang.First(m => m.ctgh_MaSanPham == id);
            db.ChiTietGioHang.Remove(sp);
            UpdateModel(sp);
            db.SaveChanges();
            return RedirectToAction("Cart");
        }
        #endregion

        #region[CheckOut]

        public ActionResult CheckOut()
        {
            List<ChiTietGioHang> ctgh_List = db.ChiTietGioHang.ToList();
            List<SanPham> sp_List = db.SanPham.ToList();
            List<GioHang> gh = new List<GioHang>();
            List<TaiKhoan> tk_List = db.TaiKhoan.ToList();
            for (int i = 0; i < ctgh_List.Count(); i++)
            {
                int Tong = 0;
                if (ctgh_List[i].ctgh_KhachHang == BienToanCuc.tkdn.tk_Id)
                {
                    GioHang Temp = new GioHang();
                    Temp.ctgh_SoLuong = ctgh_List[i].ctgh_SoLuong;
                    for (int j = 0; j < sp_List.Count(); j++)
                    {
                        if (ctgh_List[i].ctgh_MaSanPham == sp_List[j].sp_Id)
                        {
                            Temp.ctgh_SanPham = sp_List[j];
                        }
                    }
                    for (int k = 0;k < tk_List.Count();k++)
                    {
                        if (tk_List[k].tk_Id == BienToanCuc.tkdn.tk_Id)
                        {
                            Temp.ctgh_KhachHang = tk_List[k];
                        }
                    }
                    Temp.ctgh_TongTien = Temp.ctgh_SoLuong * Temp.ctgh_SanPham.sp_GiaTien;
                    Tong = Tong + Temp.ctgh_TongTien;
                    Temp.ctgh_Tong = Tong;
                    gh.Add(Temp);
                }
            }
          
                return View(gh);
        }
      

        
        [HttpPost]
        public ActionResult CheckOut(FormCollection collection, PhieuDatHang pdh)
        {
            List<PhieuDatHang> tam = db.PhieuDatHang.ToList();
            int stt = 0;
            if (tam.Count() !=0)
            {
                 stt = tam[tam.Count() - 1].pdh_Id;
            }
       
            for(int i=0;i<BienToanCuc.GioHang.Count();i++)
            {
                ChiTietPhieuDatHang ctpdh = new ChiTietPhieuDatHang();
                ctpdh.ctpdh_MaSanPham = BienToanCuc.GioHang[i].ctgh_SanPham.sp_Id;
                ctpdh.ctpdh_SoLuong = BienToanCuc.GioHang[i].ctgh_SoLuong;
                ctpdh.ctpdh_Gia = BienToanCuc.GioHang[i].ctgh_SanPham.sp_GiaTien;
                ctpdh.ctpdh_MaPDH = stt + 1;
                db.ChiTietPhieuDatHang.Add(ctpdh);
            }
            int day = DateTime.Now.Day;
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            String Date = month.ToString() + "/" + day.ToString() + "/" + year.ToString();
            pdh.pdh_MaKH = BienToanCuc.tkdn.tk_Id;
            pdh.pdh_TinhTrang = 0;
            pdh.pdh_DiaChi = collection["[0].ctgh_KhachHang.tk_DiaChi"];
            pdh.pdh_CachThanhToan = int.Parse(collection["pay"]);
            pdh.pdh_CachVanChuyen = int.Parse(collection["ship"]);
            pdh.pdh_NgayDat = Date;
            pdh.pdh_NgayGiao = collection["date"];
            
            pdh.pdh_TongTien = BienToanCuc.GioHang[0].ctgh_Tong;
            db.PhieuDatHang.Add(pdh);
            db.SaveChanges();
            return RedirectToAction("Success");
        }
        #endregion

        #region[Contact]

        public ActionResult Contact()
        {
            ViewBag.Name = BienToanCuc.Bien;
            return View();
        }
        #endregion

        #region[About]

        public ActionResult About()
        {
            ViewBag.Name = BienToanCuc.Bien;
            return View();
        }
        #endregion

        #region[ListPDH]

        public ActionResult ListPDH()
        {
            List<PhieuDatHang> pdh = db.PhieuDatHang.ToList();
            return View(pdh);
        }
        #endregion

        #region[ThongKe]
        public int KiemTraThongKe(List<ThongTinThongKe> tk, ChiTietPhieuDatHang t)
        {
            if (tk.Count()>0)
            {
                for (int i = 0; i < tk.Count(); i++)
                {

                    if (tk[i].tk_SanPham.sp_Id == t.ctpdh_MaSanPham)
                    {
                        return i;
                    }
                }
            }
                return -1;
        }
        public ActionResult ThongKe()
        {
            List<ChiTietPhieuDatHang> ctpdh = db.ChiTietPhieuDatHang.ToList();
            List<PhieuDatHang> pdh = db.PhieuDatHang.ToList();
            List<SanPham> sp = db.SanPham.ToList();
           ThongKe thongKe = new ThongKe();
           int day = DateTime.Now.Day;
           int month = DateTime.Now.Month;
           int year = DateTime.Now.Year;
          //  Thống kê tất cả
           thongKe.All = new List<ThongTinThongKe>();
            int Tong = 0;
            for (int i = 0; i < ctpdh.Count(); i++)
            {
                if (KiemTraThongKe(thongKe.All, ctpdh[i]) == -1)
                {
                    ThongTinThongKe cur = new ThongTinThongKe();
                    cur.tk_SoLuong = ctpdh[i].ctpdh_SoLuong;
                    cur.tk_TongTien = ctpdh[i].ctpdh_Gia;
                    Tong = Tong + cur.tk_TongTien;
                    cur.tk_Tong = Tong;
                    for (int i2 = 0; i2 < sp.Count(); i2++)
                    {

                        if (sp[i2].sp_Id == ctpdh[i].ctpdh_MaSanPham)
                        {
                            cur.tk_SanPham = sp[i2];
                        }
                    }
                    thongKe.All.Add(cur);
                }
                else
                {
                    thongKe.All[KiemTraThongKe(thongKe.All, ctpdh[i])].tk_SoLuong = thongKe.All[KiemTraThongKe(thongKe.All, ctpdh[i])].tk_SoLuong + ctpdh[i].ctpdh_SoLuong;
                }
            }


            //Thống kê theo năm
            thongKe.Nam = new List<ThongTinThongKe>();
            int Tong2 = 0;
            for (int i = 0; i < ctpdh.Count(); i++)
            {
                String NgayDat="0/0/0";
                for (int j = 0; j < pdh.Count();j++ )
                {
                    if(pdh[j].pdh_Id==ctpdh[i].ctpdh_MaPDH)
                    {
                        NgayDat = pdh[j].pdh_NgayDat;
                    }
                }
                string[] Date = NgayDat.Split('/');
                if (Date[2] == year.ToString())
                {
                    if (KiemTraThongKe(thongKe.Nam, ctpdh[i]) == -1)
                    {
                        ThongTinThongKe cur = new ThongTinThongKe();
                        cur.tk_SoLuong = ctpdh[i].ctpdh_SoLuong;
                        cur.tk_TongTien = ctpdh[i].ctpdh_Gia;
                        Tong2 = Tong2 + cur.tk_TongTien;
                        cur.tk_Tong = Tong2;
                        for (int i2 = 0; i2 < sp.Count(); i2++)
                        {

                            if (sp[i2].sp_Id == ctpdh[i].ctpdh_MaSanPham)
                            {
                                cur.tk_SanPham = sp[i2];
                            }
                        }
                        thongKe.Nam.Add(cur);
                    }
                    else
                    {
                        thongKe.Nam[KiemTraThongKe(thongKe.Nam, ctpdh[i])].tk_SoLuong = thongKe.Nam[KiemTraThongKe(thongKe.Nam, ctpdh[i])].tk_SoLuong + ctpdh[i].ctpdh_SoLuong;
                    }
                }
            }
        


            //Thống kê theo tháng
            thongKe.Thang = new List<ThongTinThongKe>();
            int Tong3 = 0;
            for (int i = 0; i < ctpdh.Count(); i++)
            {
                String NgayDat = "0/0/0";
                for (int j = 0; j < pdh.Count(); j++)
                {
                    if (pdh[j].pdh_Id == ctpdh[i].ctpdh_MaPDH)
                    {
                        NgayDat = pdh[j].pdh_NgayDat;
                    }
                }
                string[] Date = NgayDat.Split('/');
                if (int.Parse(Date[0]) == month && Date[2] == year.ToString())
                {
                    if (KiemTraThongKe(thongKe.Thang, ctpdh[i]) == -1)
                    {
                        ThongTinThongKe cur = new ThongTinThongKe();
                        cur.tk_SoLuong = ctpdh[i].ctpdh_SoLuong;
                        cur.tk_TongTien = ctpdh[i].ctpdh_Gia;
                        Tong3 = Tong3 + cur.tk_TongTien;
                        cur.tk_Tong = Tong3;
                        for (int i2 = 0; i2 < sp.Count(); i2++)
                        {

                            if (sp[i2].sp_Id == ctpdh[i].ctpdh_MaSanPham)
                            {
                                cur.tk_SanPham = sp[i2];
                            }
                        }
                        thongKe.Thang.Add(cur);
                    }
                    else
                    {
                        thongKe.Thang[KiemTraThongKe(thongKe.Thang, ctpdh[i])].tk_SoLuong = thongKe.Thang[KiemTraThongKe(thongKe.Thang, ctpdh[i])].tk_SoLuong + ctpdh[i].ctpdh_SoLuong;
                    }
                }
            }



            //Thống kê theo ngày 

            thongKe.Ngay = new List<ThongTinThongKe>();
            int Tong4 = 0;
            for (int i = 0; i < ctpdh.Count(); i++)
            {
                String NgayDat = "0/0/0";
                for (int j = 0; j < pdh.Count(); j++)
                {
                    if (pdh[j].pdh_Id == ctpdh[i].ctpdh_MaPDH)
                    {
                        NgayDat = pdh[j].pdh_NgayDat;
                    }
                }
                string[] Date = NgayDat.Split('/');
                if (int.Parse(Date[1]) == day && int.Parse(Date[0]) == month && Date[2] == year.ToString())
                {
                    if (KiemTraThongKe(thongKe.Ngay, ctpdh[i]) == -1)
                    {
                        ThongTinThongKe cur = new ThongTinThongKe();
                        cur.tk_SoLuong = ctpdh[i].ctpdh_SoLuong;
                        cur.tk_TongTien = ctpdh[i].ctpdh_Gia;
                        Tong4 = Tong4 + cur.tk_TongTien;
                        cur.tk_Tong = Tong4;
                        for (int i2 = 0; i2 < sp.Count(); i2++)
                        {

                            if (sp[i2].sp_Id == ctpdh[i].ctpdh_MaSanPham)
                            {
                                cur.tk_SanPham = sp[i2];
                            }
                        }
                        thongKe.Ngay.Add(cur);
                    }
                    else
                    {
                        thongKe.Ngay[KiemTraThongKe(thongKe.Ngay, ctpdh[i])].tk_SoLuong = thongKe.Ngay[KiemTraThongKe(thongKe.Ngay, ctpdh[i])].tk_SoLuong + ctpdh[i].ctpdh_SoLuong;
                    }
                }
            }


            //Thống kê theo quý
            thongKe.Quy = new List<ThongTinThongKe>();
            int Tong5 = 0;
            for (int i = 0; i < ctpdh.Count(); i++)
            {
                String NgayDat = "0/0/0";
                for (int j = 0; j < pdh.Count(); j++)
                {
                    if (pdh[j].pdh_Id == ctpdh[i].ctpdh_MaPDH)
                    {
                        NgayDat = pdh[j].pdh_NgayDat;
                    }
                }

                int quy = 1;
                if(month==1||month==2||month==3)
                {
                    quy = 1;
                }
                if (month == 4 || month ==5|| month == 6)
                {
                    quy = 2;
                }
                if (month == 7 || month ==8 || month == 9)
                {
                    quy = 3;
                }
                if (month == 10 || month == 11 || month == 12)
                {
                    quy = 4;
                }

                int Date_Quy = 1;
                string[] Date = NgayDat.Split('/');
                if (Date[2] == year.ToString())
                {
                    if (int.Parse(Date[0]) == 1 || int.Parse(Date[0]) == 2|| int.Parse(Date[0]) == 3)
                    {
                        Date_Quy = 1;
                    }
                    if (int.Parse(Date[0]) == 4 || int.Parse(Date[0]) == 5 || int.Parse(Date[0]) == 6)
                    {
                        Date_Quy = 2;
                    } 
                    if (int.Parse(Date[0]) == 7 || int.Parse(Date[0]) == 8 || int.Parse(Date[0]) == 9)
                    {
                        Date_Quy =3;
                    }
                    if (int.Parse(Date[0]) == 10 || int.Parse(Date[0]) ==11 || int.Parse(Date[0]) == 12)
                    {
                        Date_Quy = 4;
                    }
                }


                if (quy==Date_Quy)
                {
                    if (KiemTraThongKe(thongKe.Quy, ctpdh[i]) == -1)
                    {
                        ThongTinThongKe cur = new ThongTinThongKe();
                        cur.tk_SoLuong = ctpdh[i].ctpdh_SoLuong;
                        cur.tk_TongTien = ctpdh[i].ctpdh_Gia;
                        Tong5 = Tong5 + cur.tk_TongTien;
                        cur.tk_Tong = Tong5;
                        for (int i2 = 0; i2 < sp.Count(); i2++)
                        {

                            if (sp[i2].sp_Id == ctpdh[i].ctpdh_MaSanPham)
                            {
                                cur.tk_SanPham = sp[i2];
                            }
                        }
                        thongKe.Quy.Add(cur);
                    }
                    else
                    {
                        thongKe.Quy[KiemTraThongKe(thongKe.Quy, ctpdh[i])].tk_SoLuong = thongKe.Quy[KiemTraThongKe(thongKe.Quy, ctpdh[i])].tk_SoLuong + ctpdh[i].ctpdh_SoLuong;
                    }
                }
            }
            return View(thongKe);
        }
        #endregion

    }
}
