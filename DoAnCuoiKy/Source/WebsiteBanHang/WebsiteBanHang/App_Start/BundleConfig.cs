﻿using System.Web;
using System.Web.Optimization;

namespace WebsiteBanHang
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/jquery_cur").Include(
                        "~/Scripts/jquery-1.8.2.min.js",
                        "~/Scripts/css3-mediaqueries.js",
                        "~/Scripts/modernizr-1.7.js",
                        "~/Scripts/jquery.prettyPhoto.js",
                        "~/Scripts/jquery.tipsy.js",
                        "~/Scripts/jquery.easing.1.3.js",
                        "~/Scripts/camera.js",
                        "~/Scripts/jquery.jcarousel.js",
                        "~/Scripts/jquery-hover-effect.js",
                        "~/Scripts/jquery.hoverIntent.minified.js",
                        "~/Scripts/jquery.dcmegamenu.1.3.3.js",
                        "~/Scripts/jquery.tweet.js",
                        "~/Scripts/jquery.quovolver.js",
                        "~/Scripts/custom.js"
                        ));
            bundles.Add(new StyleBundle("~/Content/css").Include(
                                               "~/Content/style.css",
                                                "~/Content/prettyPhoto.css",
                                                 "~/Content/tipsy.css",
                                                  "~/Content/camera.css",
                                                   "~/Content/jcarousel.css"
                                               ));
            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}