USE [master]
GO
/****** Object:  Database [WebSiteGuitar]    Script Date: 12/06/2014 2:34:40 CH ******/
CREATE DATABASE [WebSiteGuitar]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'WebSiteGuitar', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\WebSiteGuitar.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'WebSiteGuitar_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\WebSiteGuitar_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [WebSiteGuitar] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [WebSiteGuitar].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [WebSiteGuitar] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET ARITHABORT OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [WebSiteGuitar] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [WebSiteGuitar] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [WebSiteGuitar] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET  DISABLE_BROKER 
GO
ALTER DATABASE [WebSiteGuitar] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [WebSiteGuitar] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET RECOVERY FULL 
GO
ALTER DATABASE [WebSiteGuitar] SET  MULTI_USER 
GO
ALTER DATABASE [WebSiteGuitar] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [WebSiteGuitar] SET DB_CHAINING OFF 
GO
ALTER DATABASE [WebSiteGuitar] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [WebSiteGuitar] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [WebSiteGuitar]
GO
/****** Object:  Table [dbo].[Carts]    Script Date: 12/06/2014 2:34:40 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Carts](
	[RecordId] [int] IDENTITY(1,1) NOT NULL,
	[CartId] [nvarchar](max) NULL,
	[GuitarId] [nvarchar](max) NULL,
	[Count] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChiTietGioHang]    Script Date: 12/06/2014 2:34:40 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietGioHang](
	[ctgh_KhachHang] [int] NOT NULL,
	[ctgh_MaSanPham] [int] NOT NULL,
	[ctgh_SoLuong] [int] NOT NULL,
 CONSTRAINT [PK_ChiTietGioHang] PRIMARY KEY CLUSTERED 
(
	[ctgh_KhachHang] ASC,
	[ctgh_MaSanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChiTietPhieuDatHang]    Script Date: 12/06/2014 2:34:40 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietPhieuDatHang](
	[ctpdh_MaPDH] [int] NOT NULL,
	[ctpdh_MaSanPham] [int] NOT NULL,
	[ctpdh_Gia] [int] NOT NULL,
	[ctpdh_SoLuong] [int] NOT NULL,
 CONSTRAINT [PK_ChiTietPhieuDatHang] PRIMARY KEY CLUSTERED 
(
	[ctpdh_MaPDH] ASC,
	[ctpdh_MaSanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EdmMetadata]    Script Date: 12/06/2014 2:34:40 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EdmMetadata](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ModelHash] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderDetails]    Script Date: 12/06/2014 2:34:40 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetails](
	[OrderDetailId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[GuitarId] [nvarchar](max) NULL,
	[Quantity] [int] NOT NULL,
	[UnitPrice] [decimal](18, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Orders]    Script Date: 12/06/2014 2:34:40 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](max) NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[PostalCode] [nvarchar](max) NULL,
	[Country] [nvarchar](max) NULL,
	[Phone] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Total] [decimal](18, 2) NOT NULL,
	[OrderDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PhieuDatHang]    Script Date: 12/06/2014 2:34:40 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhieuDatHang](
	[pdh_Id] [int] IDENTITY(1,1) NOT NULL,
	[pdh_MaKH] [int] NOT NULL,
	[pdh_NgayDat] [nvarchar](50) NOT NULL,
	[pdh_NgayGiao] [nvarchar](50) NOT NULL,
	[pdh_TinhTrang] [int] NOT NULL,
	[pdh_DiaChi] [nvarchar](50) NULL,
	[pdh_TongTien] [int] NOT NULL,
	[pdh_CachVanChuyen] [int] NOT NULL,
	[pdh_CachThanhToan] [int] NOT NULL,
 CONSTRAINT [PK_PhieuDatHang] PRIMARY KEY CLUSTERED 
(
	[pdh_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SanPham]    Script Date: 12/06/2014 2:34:40 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SanPham](
	[sp_Id] [int] IDENTITY(1,1) NOT NULL,
	[sp_TenSanPham] [nvarchar](50) NOT NULL,
	[sp_GiaTien] [int] NOT NULL,
	[sp_SoLuong] [int] NOT NULL,
	[sp_Loai] [nvarchar](50) NULL,
	[sp_MauSac] [nvarchar](50) NULL,
	[sp_HinhAnh] [nvarchar](50) NULL,
 CONSTRAINT [PK_SanPham] PRIMARY KEY CLUSTERED 
(
	[sp_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 12/06/2014 2:34:40 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaiKhoan](
	[tk_Id] [int] IDENTITY(1,1) NOT NULL,
	[tk_TenNguoiDung] [nvarchar](50) NOT NULL,
	[tk_TenDangNhap] [nvarchar](50) NOT NULL,
	[tk_MatKhau] [nvarchar](50) NOT NULL,
	[tk_DiaChi] [nvarchar](50) NOT NULL,
	[tk_SoDienThoai] [nvarchar](50) NULL,
	[tk_Email] [nvarchar](50) NULL,
	[tk_Loai] [int] NOT NULL,
 CONSTRAINT [PK_TaiKhoan] PRIMARY KEY CLUSTERED 
(
	[tk_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Carts] ON 

INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (1, N'7a2e81b7-d606-45f5-adf2-cc603986625d', N'1', 1, CAST(0x0000A1E101336915 AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (2, N'0f415d71-577f-4382-b2c3-08898b0a9dd1', N'4', 1, CAST(0x0000A1E1013733A7 AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (3, N'0f415d71-577f-4382-b2c3-08898b0a9dd1', N'1', 1, CAST(0x0000A1E10138554A AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (4, N'da1f7dae-3c87-40b6-888b-4e6b92d205ed', N'2', 1, CAST(0x0000A1E1013969D0 AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (5, N'da1f7dae-3c87-40b6-888b-4e6b92d205ed', N'4', 1, CAST(0x0000A1E10139C7AC AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (6, N'07683656-f672-437e-b969-6e23843cff9e', N'1', 1, CAST(0x0000A1E1013AA573 AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (7, N'4eafc9e3-90d7-4ccd-8592-df5f1e7be642', N'1', 1, CAST(0x0000A1E1013B43BA AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (8, N'4eafc9e3-90d7-4ccd-8592-df5f1e7be642', N'7', 1, CAST(0x0000A1E1013EEAFE AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (9, N'36d365a3-a1d3-4e16-a779-f43aa1db4b54', N'7', 1, CAST(0x0000A1E10140D4BA AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (10, N'12910523-a792-4d17-8d89-7a6c3a9857d2', N'4', 1, CAST(0x0000A1E1014365A3 AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (11, N'add8e4a6-91d9-4ffb-9e5e-f0b3b9065eca', N'7', 4, CAST(0x0000A1E10146F58E AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (12, N'add8e4a6-91d9-4ffb-9e5e-f0b3b9065eca', N'2', 1, CAST(0x0000A1E10147BC1E AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (13, N'add8e4a6-91d9-4ffb-9e5e-f0b3b9065eca', N'10', 1, CAST(0x0000A1E10147F7D8 AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (14, N'add8e4a6-91d9-4ffb-9e5e-f0b3b9065eca', N'1', 3, CAST(0x0000A1E1014A48CC AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (15, N'8f150f38-ca52-4377-ae37-03652e9aa3f1', N'1', 3, CAST(0x0000A1E1015026F9 AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (16, N'8f150f38-ca52-4377-ae37-03652e9aa3f1', N'7', 1, CAST(0x0000A1E10150503A AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (17, N'91ea4341-0b4b-4408-ade4-d329ab1cd695', N'2', 2, CAST(0x0000A1E101559897 AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (29, N'c218bff7-dd38-46a0-a1c6-cb62639c5cb9', N'4', 1, CAST(0x0000A1E1015F43A0 AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (30, N'f790d943-fec6-49d0-aeba-89b3e5575a4b', N'4', 2, CAST(0x0000A1E1016003AB AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (31, N'f790d943-fec6-49d0-aeba-89b3e5575a4b', N'2', 2, CAST(0x0000A1E1016011EA AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (32, N'f790d943-fec6-49d0-aeba-89b3e5575a4b', N'1', 1, CAST(0x0000A1E101604E6E AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (37, N'e1100603-1527-4ad0-9ced-8e4efbfc711c', N'4', 1, CAST(0x0000A1E1016C8812 AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (38, N'e1100603-1527-4ad0-9ced-8e4efbfc711c', N'2', 1, CAST(0x0000A1E1016CFE2C AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (39, N'e1100603-1527-4ad0-9ced-8e4efbfc711c', N'7', 1, CAST(0x0000A1E1016D118B AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (40, N'a668a602-0762-4e09-a51f-bd9abc2d17a2', N'4', 2, CAST(0x0000A1E1016DB80F AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (47, N'837c09d3-1795-4c1c-b07d-19ac5b24ebd3', N'4', 1, CAST(0x0000A1E200C3800E AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (48, N'837c09d3-1795-4c1c-b07d-19ac5b24ebd3', N'1', 1, CAST(0x0000A1E200C386B3 AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (49, N'837c09d3-1795-4c1c-b07d-19ac5b24ebd3', N'2', 1, CAST(0x0000A1E200C38C0C AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (50, N'837c09d3-1795-4c1c-b07d-19ac5b24ebd3', N'7', 1, CAST(0x0000A1E200C3A2EB AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (55, N'18b7eae5-fb27-44e5-bfd4-fb202545ccba', N'1', 1, CAST(0x0000A1E200D981AD AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (56, N'18b7eae5-fb27-44e5-bfd4-fb202545ccba', N'4', 3, CAST(0x0000A1E200D9F790 AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (129, N'3f6e1ad9-446f-4c0a-84c8-3ca672232234', N'4', 1, CAST(0x0000A1E30145D2C9 AS DateTime))
INSERT [dbo].[Carts] ([RecordId], [CartId], [GuitarId], [Count], [DateCreated]) VALUES (130, N'3f6e1ad9-446f-4c0a-84c8-3ca672232234', N'1', 1, CAST(0x0000A1E30146933B AS DateTime))
SET IDENTITY_INSERT [dbo].[Carts] OFF
INSERT [dbo].[ChiTietGioHang] ([ctgh_KhachHang], [ctgh_MaSanPham], [ctgh_SoLuong]) VALUES (0, 20, 1)
INSERT [dbo].[ChiTietGioHang] ([ctgh_KhachHang], [ctgh_MaSanPham], [ctgh_SoLuong]) VALUES (2, 8, 1)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2011, 6, 1200000, 3)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2011, 12, 15000000, 1)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2011, 16, 1900000, 2)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2012, 13, 1300000, 2)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2012, 19, 5000000, 1)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2013, 18, 1500000, 1)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2014, 11, 20000000, 2)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2014, 13, 1300000, 3)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2015, 6, 1200000, 1)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2015, 13, 1300000, 1)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2015, 16, 1900000, 1)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2016, 6, 1200000, 1)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2016, 8, 2000000, 1)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2016, 13, 1300000, 1)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2016, 16, 1900000, 1)
INSERT [dbo].[ChiTietPhieuDatHang] ([ctpdh_MaPDH], [ctpdh_MaSanPham], [ctpdh_Gia], [ctpdh_SoLuong]) VALUES (2017, 8, 2000000, 1)
SET IDENTITY_INSERT [dbo].[EdmMetadata] ON 

INSERT [dbo].[EdmMetadata] ([Id], [ModelHash]) VALUES (1, N'67DB23A8BF2B7301C75AFA8689010D2F0A9F2AA3DB7663EC4C4B8B7BBA378B29')
SET IDENTITY_INSERT [dbo].[EdmMetadata] OFF
SET IDENTITY_INSERT [dbo].[PhieuDatHang] ON 

INSERT [dbo].[PhieuDatHang] ([pdh_Id], [pdh_MaKH], [pdh_NgayDat], [pdh_NgayGiao], [pdh_TinhTrang], [pdh_DiaChi], [pdh_TongTien], [pdh_CachVanChuyen], [pdh_CachThanhToan]) VALUES (2011, 2, N'5/11/2013', N'6/11/2013', 0, N'45/Điện Biên Phủ/Quận 3', 3600000, 0, 0)
INSERT [dbo].[PhieuDatHang] ([pdh_Id], [pdh_MaKH], [pdh_NgayDat], [pdh_NgayGiao], [pdh_TinhTrang], [pdh_DiaChi], [pdh_TongTien], [pdh_CachVanChuyen], [pdh_CachThanhToan]) VALUES (2012, 2, N'1/11/2014', N'01/12/2014', 0, N'45/Điện Biên Phủ/Quận 3', 2600000, 1, 0)
INSERT [dbo].[PhieuDatHang] ([pdh_Id], [pdh_MaKH], [pdh_NgayDat], [pdh_NgayGiao], [pdh_TinhTrang], [pdh_DiaChi], [pdh_TongTien], [pdh_CachVanChuyen], [pdh_CachThanhToan]) VALUES (2013, 2, N'4/8/2014', N'4/9/2014', 0, N'45/Điện Biên Phủ/Quận 3', 1500000, 2, 0)
INSERT [dbo].[PhieuDatHang] ([pdh_Id], [pdh_MaKH], [pdh_NgayDat], [pdh_NgayGiao], [pdh_TinhTrang], [pdh_DiaChi], [pdh_TongTien], [pdh_CachVanChuyen], [pdh_CachThanhToan]) VALUES (2014, 2, N'6/11/2014', N'6/11/2014', 0, N'45/Điện Biên Phủ/Quận 3', 40000000, 2, 0)
INSERT [dbo].[PhieuDatHang] ([pdh_Id], [pdh_MaKH], [pdh_NgayDat], [pdh_NgayGiao], [pdh_TinhTrang], [pdh_DiaChi], [pdh_TongTien], [pdh_CachVanChuyen], [pdh_CachThanhToan]) VALUES (2015, 2, N'6/11/2014', N'6/11/2014', 0, N'45/Điện Biên Phủ/Quận 3', 1200000, 0, 0)
INSERT [dbo].[PhieuDatHang] ([pdh_Id], [pdh_MaKH], [pdh_NgayDat], [pdh_NgayGiao], [pdh_TinhTrang], [pdh_DiaChi], [pdh_TongTien], [pdh_CachVanChuyen], [pdh_CachThanhToan]) VALUES (2016, 2, N'6/12/2014', N'6/12/2014', 0, N'45/Điện Biên Phủ/Quận 3', 1200000, 0, 0)
INSERT [dbo].[PhieuDatHang] ([pdh_Id], [pdh_MaKH], [pdh_NgayDat], [pdh_NgayGiao], [pdh_TinhTrang], [pdh_DiaChi], [pdh_TongTien], [pdh_CachVanChuyen], [pdh_CachThanhToan]) VALUES (2017, 2, N'6/12/2014', N'06/20/2014', 0, N'45/Điện Biên Phủ/Quận 3', 2000000, 1, 1)
SET IDENTITY_INSERT [dbo].[PhieuDatHang] OFF
SET IDENTITY_INSERT [dbo].[SanPham] ON 

INSERT [dbo].[SanPham] ([sp_Id], [sp_TenSanPham], [sp_GiaTien], [sp_SoLuong], [sp_Loai], [sp_MauSac], [sp_HinhAnh]) VALUES (4, N'Fender R2', 1700000, 12, N'Bass', N'Vàng', N'\Images\products\Bass\1a.jpg')
INSERT [dbo].[SanPham] ([sp_Id], [sp_TenSanPham], [sp_GiaTien], [sp_SoLuong], [sp_Loai], [sp_MauSac], [sp_HinhAnh]) VALUES (6, N'Modern K2', 1200000, 8, N'Bass', N'Hồng', N'\Images\products\Bass\2a.jpg')
INSERT [dbo].[SanPham] ([sp_Id], [sp_TenSanPham], [sp_GiaTien], [sp_SoLuong], [sp_Loai], [sp_MauSac], [sp_HinhAnh]) VALUES (8, N'Modern I3', 2000000, 5, N'Classic', N'Nâu', N'\Images\products\Classic\1a.jpg')
INSERT [dbo].[SanPham] ([sp_Id], [sp_TenSanPham], [sp_GiaTien], [sp_SoLuong], [sp_Loai], [sp_MauSac], [sp_HinhAnh]) VALUES (9, N'Modern RS', 2100000, 9, N'Acaustic', N'Nâu', N'\Images\products\acoustic\2a.jpg')
INSERT [dbo].[SanPham] ([sp_Id], [sp_TenSanPham], [sp_GiaTien], [sp_SoLuong], [sp_Loai], [sp_MauSac], [sp_HinhAnh]) VALUES (10, N'Ferder ZX', 700000, 23, N'Classic', N'Nâu', N'\Images\products\Classic\2a.jpg')
INSERT [dbo].[SanPham] ([sp_Id], [sp_TenSanPham], [sp_GiaTien], [sp_SoLuong], [sp_Loai], [sp_MauSac], [sp_HinhAnh]) VALUES (11, N'Taylor KL', 20000000, 3, N'Classic', N'Nâu', N'\Images\products\Classic\3a.jpg')
INSERT [dbo].[SanPham] ([sp_Id], [sp_TenSanPham], [sp_GiaTien], [sp_SoLuong], [sp_Loai], [sp_MauSac], [sp_HinhAnh]) VALUES (12, N'Taylor CH', 15000000, 4, N'Điện', N'Nâu', N'\Images\products\Dien\1a.jpg')
INSERT [dbo].[SanPham] ([sp_Id], [sp_TenSanPham], [sp_GiaTien], [sp_SoLuong], [sp_Loai], [sp_MauSac], [sp_HinhAnh]) VALUES (13, N'Suzuki RE', 1300000, 8, N'Điện', N'Nâu', N'\Images\products\Dien\2a.jpg')
INSERT [dbo].[SanPham] ([sp_Id], [sp_TenSanPham], [sp_GiaTien], [sp_SoLuong], [sp_Loai], [sp_MauSac], [sp_HinhAnh]) VALUES (14, N'Suzuki GH', 2300000, 14, N'Bass', N'Nâu', N'\Images\products\Bass\3a.jpg')
INSERT [dbo].[SanPham] ([sp_Id], [sp_TenSanPham], [sp_GiaTien], [sp_SoLuong], [sp_Loai], [sp_MauSac], [sp_HinhAnh]) VALUES (15, N'Taylor DK', 13000000, 6, N'Classic', N'Nâu', N'\Images\products\Classic\4a.jpg')
INSERT [dbo].[SanPham] ([sp_Id], [sp_TenSanPham], [sp_GiaTien], [sp_SoLuong], [sp_Loai], [sp_MauSac], [sp_HinhAnh]) VALUES (16, N'Suzuki LK100', 1900000, 12, N'Điện', N'Nâu', N'\Images\products\Dien\3a.jpg')
INSERT [dbo].[SanPham] ([sp_Id], [sp_TenSanPham], [sp_GiaTien], [sp_SoLuong], [sp_Loai], [sp_MauSac], [sp_HinhAnh]) VALUES (17, N'Suzuki VH30', 900000, 23, N'Bass', N'Nâu', N'\Images\products\Bass\4a.jpg')
INSERT [dbo].[SanPham] ([sp_Id], [sp_TenSanPham], [sp_GiaTien], [sp_SoLuong], [sp_Loai], [sp_MauSac], [sp_HinhAnh]) VALUES (18, N'Suzuki 500', 1500000, 12, N'Acaustic', N'Nâu', N'\Images\products\acoustic\3a.jpg')
INSERT [dbo].[SanPham] ([sp_Id], [sp_TenSanPham], [sp_GiaTien], [sp_SoLuong], [sp_Loai], [sp_MauSac], [sp_HinhAnh]) VALUES (19, N'Modern E20', 5000000, 5, N'Điện', N'Nâu', N'\Images\products\Dien\4a.jpg')
SET IDENTITY_INSERT [dbo].[SanPham] OFF
SET IDENTITY_INSERT [dbo].[TaiKhoan] ON 

INSERT [dbo].[TaiKhoan] ([tk_Id], [tk_TenNguoiDung], [tk_TenDangNhap], [tk_MatKhau], [tk_DiaChi], [tk_SoDienThoai], [tk_Email], [tk_Loai]) VALUES (1, N'Đặng Tiến Dũng', N'dungctt101', N'123', N'23/CMT8/Quận 3/TPHCM', N'0972248599', N'dandtiendung@yahoo.com', 0)
INSERT [dbo].[TaiKhoan] ([tk_Id], [tk_TenNguoiDung], [tk_TenDangNhap], [tk_MatKhau], [tk_DiaChi], [tk_SoDienThoai], [tk_Email], [tk_Loai]) VALUES (2, N'Ngô Lương Hoàn Duy', N'hoanduy', N'321', N'45/Điện Biên Phủ/Quận 3', N'0987654321', N'duy@yahoo.com', 1)
SET IDENTITY_INSERT [dbo].[TaiKhoan] OFF
ALTER TABLE [dbo].[OrderDetails]  WITH CHECK ADD  CONSTRAINT [OrderDetail_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([OrderId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderDetails] CHECK CONSTRAINT [OrderDetail_Order]
GO
USE [master]
GO
ALTER DATABASE [WebSiteGuitar] SET  READ_WRITE 
GO
